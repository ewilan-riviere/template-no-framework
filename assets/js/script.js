function clearSearch() {
    setTimeout(function () {
        $('form[name="q"]').submit();
        $('input[type="search"], textarea').val('');
    }, 2000);
}

$('#sidebar-top').click(function () {
    $('#sidebar-top').toggleClass('sidebar-top_activate');
    $('#content').toggleClass('margin_content');
    $('#sidebar').toggleClass('active');
    $('#sidebarCollapse').toggleClass('active');
});