<footer class="bg-white footer footer_shadow">
    <p class="text-dark vertical-center text-center">
        <span class="font-weight-bold">
            <?php
                echo $title;
            ?>
        </span>
        · 
        <a class="font-weight-bold text-dark" href="https://github.com/ewilan-riviere/template-no-framework" target="_blank">
            <i class="fab fa-github"></i> Dépôt GitHub
        </a>
        <br>
        <i>
            © Copyright 2019, 
            <?php
                echo " ".$author;
            ?>
            . All rights reserved.
        </i>
    </p>
</footer>