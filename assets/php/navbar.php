<nav class="navbar navbar-expand sticky-top navbar-light bg-light shadow-sm bg-white">
	<a class="navbar-brand" href="">
		<img class="d-inline-block align-top" width="30" height="30" src="assets/images/<?php echo $logo ?>"> 
		<span class="navbar_title">
			<?php
				echo $title;
			?>
		</span>
	</a>

    <div class="navbar-collapse" id="navbarTemplate">
        <ul class="navbar-nav mr-auto">
            <!-- Main Navbar -->
            <?php
                foreach ($navbar_arr as $key => $value) {
                    if ($key == 'Navbar') {
                        foreach ($value as $k => $v) {
            ?>
                            <li class="nav-item font-weight-bold">
                                <a id="<?php echo $k ?>" class="nav-link" target="_blank" href="<?php echo $k ?>">
                                    <?php echo $v ?>
                                </a>
                            </li>
            <?php
                        }
                    }
                }
            ?>
            <!-- dropdown Navbar -->
            <?php
            for ($i=0; $i < sizeof($dropdown); $i++) { 
                    foreach ($navbar_arr as $key => $value) {
                        foreach ($value as $k => $v) {
                            if ($k == $dropdown[$i].'_icon') {
                                $icon = $v;
                            }
                            if ($k == $dropdown[$i]) { 
                            ?>
                                <li class="nav-item font-weight-bold dropdown">
										<a id="<?php echo $k ?>"
											class="nav-link dropdown-toggle"
                                            href="#" id="dropdown"
                                            role="button"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false" >
                                            <?php echo $icon.' '.$k ?>
                                        </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdown">
                                    <?php
                                    foreach ($v as $k2 => $v2) {
                                    ?>
                                        <a class="dropdown-item"
                                            target="_blank"
                                            href="<?php echo $k2 ?>">
                                            <?php echo $v2 ?>
                                        </a>
                                    <?php
                                    } ?>
                                    </div>
                                </li>
                        <?php 
                            }
                        }
                    }
                }
                ?>
        </ul>
        <?php
            foreach ($navbar_form as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($k == 'name') {
                        $key_low = str_replace(' ','-',strtolower($v));
                        $key_title = $v;
                    }
                    if ($k == 'link') {
                        $link = $v;
                    }
                }
        ?>
			<form target="_blank" class="form-inline mx-2" action="<?php echo $link ?>" methode="GET">
				<div class="input-group">
					<input class="form-control search_input input_radius input_border"
						type="search"
						name="q"
						placeholder="<?php echo $key_title ?>"
						onclick="clearSearch();"
						aria-label="Search">
					<div class="input-group-append">
						<button class="input-group-text input_radius_logo input_logo_border" type="submit" id="search-<?php echo $key_low ?>">
							<?php if($k == 'logo') echo $v ?>
						</button>
					</div>
				</div>
			</form>
        <?php
            }
        ?>
    </div>
</nav>