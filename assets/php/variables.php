<?php
$title = 'Template';
$author = 'Ewilan Rivière';

$dir    = '/var/www/html/';

$logo = 'logo.png';

// dropdown
// Add title of dropdown
$dropdown = [ "Documentation", "Git" , "Serveurs" ];

$navbar_arr = array(
    'Navbar' => array(
        'home' => '<i class="fas fa-home"></i> Accueil',
        'sign-up' => '<i class="fas fa-user-plus"></i> Inscription',
        'sign-in' => '<i class="fas fa-sign-in-alt"></i> Connexion',
    ),
    'Dropdown' => array(
        // Add icon like this 'dropdown_icon' => '<i class="fa fa></i>
        'Git_icon' => '<i class="fab fa-git-alt"></i>',
        // Add array for dropdown
        'Git' => array(
            'https://github.com/' => '<i class="fab fa-github"></i> GitHub',
            'https://gitlab.com/' => '<i class="fab fa-gitlab"></i> GitLab',
            'https://framagit.org/' => '<i class="fab fa-gitlab"></i> Framagit',
            'https://bitbucket.org/' => '<i class="fab fa-bitbucket"></i> BitBucket',
        ),
    ),
);

$navbar_form = array(
    [
        'name' => 'Google',
        'link' => 'https://www.google.com/search',
        'logo' => '<i class="fab fa-google"></i>'
    ]
);