<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
		<?php
			echo $title;
		?>
	</title>
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/navbar-side.css">
	<link rel="stylesheet" href="assets/css/responsive.css">
	<script>
		function activeNav(id) {
			var element = document.getElementById(id);
			element.classList.add('active');
			element.classList.add('disabled');
		}
	</script>
	<script src="assets/js/font-awesome/font-awesome.min.js"></script>
	<link rel="icon" type="image/png" href="assets/images/<?php echo $logo ?>" />
</head>
<body class="parallax">