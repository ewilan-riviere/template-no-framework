<style>
    .social_menu {
        margin-right: 0 !important;
    }
</style>
<nav id="sidebar" class="active">
    <div class="sidebar-header">
        <h3>Bootstrap Sidebar</h3>
        <img width="30%" class="mx-auto" src="{{ asset('../image/logo/ewilan_logo.png') }}" alt="ER">
    </div>

    <ul class="list-unstyled components">
        <p></p>
        <li class="active">
            <a href="#">Accueil</a>
        </li>
        <li>
            <a href="#">Portfolio</a>
        </li>
        <li>
            <a href="#">Contact</a>
        </li>
        <li>
            <a href="#">À propos</a>
        </li>
    </ul>
    <!-- <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pages</a>
        <ul class="collapse list-unstyled" id="pageSubmenu">
            <li>
                <a href="#">Page 1</a>
            </li>
            <li>
                <a href="#">Page 2</a>
            </li>
            <li>
                <a href="#">Page 3</a>
            </li>
        </ul> -->

    <ul class="list-unstyled CTAs">
        <li class="list-unstyled list-inline">
            <ul class="list-unstyled list-inline">
                <li class="d-inline-block">
                    <a href="https://github.com/ewilan-riviere" target="_blank" class="font-weight-light" data-toggle="tooltip" data-placement="top" title="GitHub">
                        <i class="fab fa-github social_github fa-2x"></i>
                    </a>
                </li>
                <li class="d-inline-block">
                    <a href="https://www.linkedin.com/in/ewilan-riviere/" target="_blank" class="font-weight-light" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                        <i class="fab fa-linkedin social_linkedin fa-2x"></i>
                    </a>
                </li>
                <li class="d-inline-block">
                    <a href="mailto:ewilan@dotslashplay.it" class="font-weight-light" data-toggle="tooltip" data-placement="top" title="E-Mail">
                        <i class="fas fa-envelope social_envelope fa-2x"></i>
                    </a>
                </li>
                <li class="d-inline-block">
                    <a href="documents/ewilan-riviere_cv.pdf" target="_blank" class="font-weight-light" data-toggle="tooltip" data-placement="top" title="CV">
                        <i class="fas fa-graduation-cap social_graduate fa-2x"></i>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>
        </li>
        <li>
            <a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a>
        </li>
    </ul>
</nav>