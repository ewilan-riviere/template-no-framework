<?php
	include 'assets/php/variables.php';
	include 'assets/php/head.php';
?>
<div id="navbarSide">
	<?php
		include 'assets/php/navbar-side.php';
		include 'assets/php/navbar-side_icon.php';
	?>
</div>
<div id="navbarClassic">
	<?php
		include 'assets/php/navbar.php';
	?>
</div>
<div class="d-flex all_page">
	<div class="m-auto pb-2">
		<h1 class="m-auto pb-4">
			<span id="template" class="navbar_title">
				<?php
					echo $title;
				?>
			</span>
		</h1>
		<img class="index_image_intro" src="assets/images/<?php echo $logo ?>" alt="logo">
	</div>
</div>

<script>
	activeNav('home');
</script>

<?php
	include 'assets/php/footer.php';
    include 'assets/php/foot.php';
?>