# Template with No Framework
![php](assets/images/php-logo.png)  
*You need a simple web app without install a heavy framework ?*  

>This template offer somes basics features to display a basic web app just with one file to edit.

## Customize
Edit `variables.php` into `assets/php/` to edit somes variables to customize this template before deploy it.

*Details of variables*